<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;


use App\Modules\Backend\Contestant\Repositories\ContestantRepository;
use App\Modules\Backend\Event\Repositories\EventRepository;
use App\Modules\Backend\Event\Requests\CreateEventRequest;
use App\Modules\Backend\Event\Requests\UpdateEventRequest;
use Illuminate\Http\Request;
//use Illuminate\Contracts\Logging\Log;
use Psr\Log\LoggerInterface as Log;
use Yajra\DataTables\Facades\DataTables;

class ContestantController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Log $log, ContestantRepository $contestantRepository)
    {
        $this->contestantRepository = $contestantRepository;
        $this->middleware('auth');
        $this->log = $log;
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $this->contestantRepository->getModel();
        if (\request()->ajax()) {
            $contestants = $this->contestantRepository->getAll();
            return DataTables::of($contestants)
                ->editColumn('action', function ($contestants) {
                    $data = $contestants;
                    $name = 'contestant';
                    $view = false;
                    return $this->view('partials.common.action', compact('data', 'name', 'view'));
                })
                ->editColumn('is_winner', function ($contestants) {
                    $data = $contestants;
                    return $this->view('partials.common.iswinner', compact('data'));
                })
//                ->editColumn('event_image', function ($contestants) {
//                    $url=asset($contestants->getImage());
//                    return '<img src='.$url.' border="0" width="40"  />';
//                        return '<img src="'.asset($contestants->getImage()).'" border="0" width="40" class="img-rounded" align="center" />';
//                })
                ->editColumn('id', 'ID: {{$id}}')
//                    ->rawColumns(['banner_image'])
//                ->rawColumns(['event_image', 'action'])
                ->make(true);

        }
        return $this->view('website.contestant.index');

    }


}

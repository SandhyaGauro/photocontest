<?php

namespace App\Http\Controllers\Admin;

use App\Contestant;
use App\Exhibition;
use App\ExhibitionImage;
use App\Feedback;
use App\Http\Controllers\Controller;
use App\Modules\Backend\Exhibition\Repositories\ExhibitionRepository;
use App\Modules\Backend\Exhibition\Requests\CreateExhibitionRequest;
use App\Modules\Backend\Exhibition\Requests\UpdateExhibitionRequest;
use App\Modules\Backend\ExhibitionImage\Repositories\ExhibitionImageRepository;
use App\User;
use Illuminate\Http\Request;
//use Illuminate\Contracts\Logging\Log;
use Psr\Log\LoggerInterface as Log;
use Yajra\DataTables\Facades\DataTables;

class ExhibitionController extends BaseController
{
    private $exhibitionRepository, $exhibitionImageRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Log $log, ExhibitionRepository $exhibitionRepository, ExhibitionImageRepository $exhibitionImageRepository)
    {
        $this->exhibitionRepository = $exhibitionRepository;
        $this->exhibitionImageRepository = $exhibitionImageRepository;
        $this->middleware('auth');
        $this->log = $log;
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $this->exhibitionRepository->getModel();
        if (\request()->ajax()) {

            $events = $this->exhibitionRepository->getAll();

            return DataTables::of($events)
                ->editColumn('action', function ($events) {
                    $data = $events;
                    $name = 'admin.exhibitions';
//                    $view = true;
                    return $this->view('partials.common.action', compact('data', 'name', 'view'));
                })
                ->editColumn('event_image', function ($events) {
                    $url = asset($events->getImage());
                    return '<img src=' . $url . ' border="0" width="40"  />';
//                        return '<img src="'.asset($events->getImage()).'" border="0" width="40" class="img-rounded" align="center" />';
                })
                ->editColumn('id', 'ID: {{$id}}')
//                    ->rawColumns(['banner_image'])
                ->rawColumns(['event_image', 'action'])
                ->make(true);

        }
        return $this->view('website.exhibition.index');

    }

    public function create()
    {
        return $this->view('website.exhibition.create');

    }

    public function store(CreateExhibitionRequest $createExhibitionRequest)
    {
        $this->exhibitionRepository->getModel();
        $data = $createExhibitionRequest->all();
        $image_data = $createExhibitionRequest->only('s_image', 's_caption');
        foreach ($image_data as $key => $s) {
            $key = substr($key, 2);
            $images_data[$key] = $s;
        }

        try {
            $event = $this->exhibitionRepository->createWithDetails(['data' => $data, 'images_data' => $images_data]);
            if ($event == false) {
                session()->flash('danger', 'Oops! Something went wrong.');
                return redirect()->back()->withInput();
            }
            session()->flash('success', 'Exhibition created successfully');
            return redirect()->route('admin.exhibitions.index');
        } catch (\Exception $e) {
            $this->log->error('Exhibition create : ' . $e->getMessage());
            session()->flash('danger', 'Oops! Something went wrong.');
            return redirect()->back()->withInput();
        }
    }

    public function edit($id)
    {

        $this->exhibitionRepository->getModel();
        $event = $this->exhibitionRepository->findById($id);
        $images = $this->exhibitionImageRepository->findBy('exhibition_id', $id, '=', false);

        return $this->view('website.exhibition.edit', compact('event', 'images'));
    }

    public function show($id)
    {
//        $contestant=Contestant::where('event_id','=',$id)->orderBy('vote','desc')->get();
        return $this->view('website.event.contestant', compact('id'));

    }

    public function ajaxContestantEvent($id)
    {
        $contestants = Contestant::where('event_id', '=', $id)->orderBy('vote', 'desc')->get();
        return DataTables::of($contestants)
            ->editColumn('action', function ($contestants) {
                $data = $contestants;
                $name = 'event.contestant';
                $view = false;
                $edit = false;
                return $this->view('partials.common.action2', compact('data', 'name', 'view', 'edit'));
            })
            ->editColumn('is_winner', function ($contestants) {
                if ($contestants->is_winner == 1)
                    return '<span class="label label-success">Winner</span>';

                return '<span class="label label-warning">Failed</span>';
//               $data = $contestants;
//               return $this->view('partials.common.iswinner', compact('data'));
            })
            ->editColumn('image', function ($contestants) {
                $url = asset($contestants->getImage());
                return '<img src=' . $url . ' border="0" width="80" height="80" />';
//                        return '<img src="'.asset($contestants->getImage()).'" border="0" width="40" class="img-rounded" align="center" />';
            })
            ->editColumn('id', 'ID: {{$id}}')
//                    ->rawColumns(['banner_image'])
            ->rawColumns(['image', 'action', 'is_winner'])
            ->make(true);

    }

    public function winnerSelect($id)
    {
        $winnerUpdate = Contestant::where('id', '=', $id)->first();
        $winnerUpdate->is_winner = 1;
        $winnerUpdate->save();
        session()->flash('success', 'Event created successfully');
        return redirect()->back()->withInput();

    }

    public function update(UpdateExhibitionRequest $updateExhibitionRequest, $id)
    {
        $this->exhibitionRepository->getModel();
        $data = $updateExhibitionRequest->all();
        $image_data = $updateExhibitionRequest->only('s_id', 's_image', 's_caption');
        foreach ($image_data as $key => $s) {
            $key = substr($key, 2);
            $images_data[$key] = $s;
        }

        try {
            $event = $this->exhibitionRepository->updateWithDetails(['data' => $data, 'images_data' => $images_data], $id);
            if ($event == false) {
                session()->flash('danger', 'Oops! Something went wrong.');
                return redirect()->back()->withInput();
            }
            session()->flash('success', 'Exhibition updated successfully');
            return redirect()->route('admin.exhibitions.index');
        } catch (\Exception $e) {
            $this->log->error('Exhibition update : ' . $e->getMessage());
            session()->flash('danger', 'Oops! Something went wrong.');
            return redirect()->back()->withInput();
        }
    }

    public function feedback()
    {
        Feedback::all();
        if (\request()->ajax()) {

            $events = Feedback::all();

            return DataTables::of($events)
                ->editColumn('action', function ($events) {
                    $data = $events;
                    $name = 'admin.feedback';
//                    $view = true;
                    return $this->view('partials.common.action', compact('data', 'name', 'view'));
                })
                ->editColumn('exhibition_id', function ($events) {
                    $exhibition = Exhibition::where('id', '=', $events->exhibition_id)->first();
                    return $exhibition['name'];
                })
                ->editColumn('user_id', function ($events) {
                    $user = User::where('id', '=', $events->user_id)->first();
                    return $user['name'];
                })
                ->editColumn('id', 'ID: {{$id}}')
//                    ->rawColumns(['banner_image'])
                ->rawColumns(['exhibition_id', 'action'])
                ->rawColumns(['user_id', 'action'])
                ->make(true);

        }
        return $this->view('website.exhibition.feedback');
    }

    public function destroy($id)
    {

        $event = Exhibition::find($id);
        $event->delete();
        session()->flash('message', 'Exhibition deleted successfully');
        return redirect()->back()->withInput();
    }
}

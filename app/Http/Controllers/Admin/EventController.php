<?php

namespace App\Http\Controllers\Admin;

use App\Contestant;
use App\Event;
use App\Http\Controllers\Controller;


use App\Modules\Backend\Event\Repositories\EventRepository;
use App\Modules\Backend\Event\Requests\CreateEventRequest;
use App\Modules\Backend\Event\Requests\UpdateEventRequest;
use Illuminate\Http\Request;
//use Illuminate\Contracts\Logging\Log;
use Psr\Log\LoggerInterface as Log;
use Yajra\DataTables\Facades\DataTables;

class EventController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Log $log, EventRepository $eventRepository)
    {
        $this->eventRepository = $eventRepository;
        $this->middleware('auth');
        $this->log = $log;
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $this->eventRepository->getModel();
        if(\request()->ajax()) {
            $events = $this->eventRepository->getAll();
            return DataTables::of($events)
                ->editColumn('action', function ($events) {
                    $data = $events;
                    $name = 'event';
                    $view = true;
                    return $this->view('partials.common.action', compact('data', 'name', 'view'));
                })
                ->editColumn('event_image', function ($events) {
                    $url=asset($events->getImage());
                    return '<img src='.$url.' border="0" width="40"  />';
//                        return '<img src="'.asset($events->getImage()).'" border="0" width="40" class="img-rounded" align="center" />';
                })
                ->editColumn('id', 'ID: {{$id}}')
//                    ->rawColumns(['banner_image'])
                ->rawColumns(['event_image', 'action'])
                ->make(true);

        }
        return $this->view('website.event.index');

    }

    public function create()
    {
        return $this->view('website.event.create');

    }

    public function store(CreateEventRequest $createEventRequest)
    {
        
        $this->eventRepository->getModel();
        $data = $createEventRequest->all();
        $data['image'] = $data['event'];
        try {
            $event = $this->eventRepository->create($data);
            if ($event == false) {
                session()->flash('danger', 'Oops! Something went wrong.');
                return redirect()->back()->withInput();
            }
            session()->flash('success', 'Event created successfully');
            return redirect()->route('event.index');
        } catch (\Exception $e) {
            $this->log->error('Event create : ' . $e->getMessage());
            session()->flash('danger', 'Oops! Something went wrong.');
            return redirect()->back()->withInput();
        }
    }
    public function edit($id)
    {

         $this->eventRepository->getModel();
        $event = $this->eventRepository->findById($id);

        return $this->view('website.event.edit', compact('event'));
    }
    public function show($id){
//        $contestant=Contestant::where('event_id','=',$id)->orderBy('vote','desc')->get();
        return $this->view('website.event.contestant', compact('id'));

   }
   public function ajaxContestantEvent($id){
        $contestants=Contestant::where('event_id','=',$id)->orderBy('vote','desc')->get();
       return DataTables::of($contestants)
           ->editColumn('action', function ($contestants) {
               $data = $contestants;
               $name = 'event.contestant';
               $view = false;
               $edit=false;
               return $this->view('partials.common.action2', compact('data', 'name', 'view','edit'));
           })
           ->editColumn('is_winner', function ($contestants) {
               if($contestants->is_winner==1)
              return  '<span class="label label-success">Winner</span>';

               return '<span class="label label-warning">Failed</span>';
//               $data = $contestants;
//               return $this->view('partials.common.iswinner', compact('data'));
           })
                ->editColumn('image', function ($contestants) {
                    $url=asset($contestants->getImage());
                    return '<img src='.$url.' border="0" width="80" height="80" />';
//                        return '<img src="'.asset($contestants->getImage()).'" border="0" width="40" class="img-rounded" align="center" />';
                })
           ->editColumn('id', 'ID: {{$id}}')
//                    ->rawColumns(['banner_image'])
                ->rawColumns(['image', 'action','is_winner'])
           ->make(true);

   }
   public function winnerSelect($id)
   {
        $winnerUpdate=Contestant::where('id','=',$id)->first();
        $winnerUpdate->is_winner=1;
        $winnerUpdate->save();
       session()->flash('success', 'Winner selected successfully');
       return redirect()->back()->withInput();

   }
   public function winnerReject($id)
   {

        $winnerUpdate=Contestant::where('id','=',$id)->first();
        $winnerUpdate->is_winner=0 ;
        $winnerUpdate->save();
       session()->flash('success', 'Winner rejected successfully');
       return redirect()->back()->withInput();

   }

    public function update(UpdateEventRequest $updateEventRequest, $id)
    {
         $this->eventRepository->getModel();
        $data = $updateEventRequest->all();
        $data['image']=$data['event'];
        try {
            $event = $this->eventRepository->update($data, $id);
            if($event == false) {
                session()->flash('danger', 'Oops! Something went wrong.');
                return redirect()->back()->withInput();
            }
            session()->flash('success', 'Event updated successfully');
            return redirect()->route('dashboard.banners.index');
        }
        catch (\Exception $e) {
            $this->log->error('Event update : '.$e->getMessage());
//            session()->flash('danger', 'Oops! Something went wrong.');
            return redirect()->back()->withInput();
        }
    }
    public function destroy($id)
    {

        $event = Event::find($id);
        $event->delete();
        session()->flash('message', 'Event deleted successfully');
        return redirect()->route('event.index');
    }

}

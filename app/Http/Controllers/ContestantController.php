<?php

namespace App\Http\Controllers;

use App\Contestant;
use App\Modules\Backend\Event\Repositories\EventRepository;
//use App\Modules\Framework\Request;
use Validator;
use Auth;
use Illuminate\Http\Request;

class ContestantController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int $id
     * @return View
     */
    public function __construct(EventRepository $eventRepository)

    {
        $this->eventRepository = $eventRepository;
    }
    public function index($id){
        $this->view_data['id']=$id;
        $login=Auth::user();
        if($login == null)
            return redirect()->route('contestant_signup')->withErrors('You have to login first to participate');
        $this->view_data['events'] = $this->eventRepository->getAll();
        return view('frontend.contestant-apply', $this->view_data);
    }
    public function applyContestant(Request $request,$id)
    {


        $user = Auth::user();
        $contestant = new Contestant();
        $contestant->user_id = $user['id'];
        $contestant->event_id = isset($id)?$id:$request['event_id'];
        $contestant->name = $request['name'];
        $contestant->description = $request['description'];
        $contestant->address = $request['address'];
        $contestant->contact_no = $request['contact_no'];

        $contestant->type = 'event';
        $contestant->image_type = $request['image_type'];
        $contestant->image=$this->fileUpload($request,'contestant');

        $contestant->save();
        return back()->with('success', 'Your form has been added successfully.');

    }
    public function applyContestant2(Request $request)
    {

        $user = Auth::user();
        $contestant = new Contestant();
        $contestant->user_id = $user['id'];
        $contestant->event_id = isset($id)?$id:$request['event_id'];
        $contestant->name = $request['name'];
        $contestant->description = $request['description'];
        $contestant->address = $request['address'];
        $contestant->contact_no = $request['contact_no'];

        $contestant->type = 'event';
        $contestant->image_type = $request['image_type'];
        $contestant->image=$this->fileUpload($request,'contestant');

        $contestant->save();
        return back()->with('success', 'Your form has been added successfully.');

    }

    public function fileUpload(Request $request, $fieldName)
    {
        $this->validate($request, array(
            'image' =>  'image',
        ));

        try{
            $path =  $request->image->store('public/'.$fieldName);
            if (!$path)
                return url('storage');
            $dirs = explode('/', $path);
            if ($dirs[0] === 'public')
                $dirs[0] = 'storage';
            $response['full_url'] = url(implode('/', $dirs));
            $response['image_name'] = ($request->image)->hashName();
            return $response['image_name'];


        }
        catch (\Exception $e)
        {
            dd($e);
        }
    }



}
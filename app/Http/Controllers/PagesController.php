<?php

namespace App\Http\Controllers;

use App\Contestant;
use App\Event;
use App\Exhibition;
use App\Feedback;
use App\Modules\Backend\Contestant\Repositories\ContestantRepository;
use App\Modules\Backend\Event\Repositories\EventRepository;
use App\User;
use App\Http\Controllers\Controller;
use App\Vote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use Auth;
use Session;

class PagesController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int $id
     * @return View
     */
    public function __construct(Contestant $contestant, EventRepository $eventRepository, ContestantRepository $contestantRepository)

    {
        $this->eventRepository = $eventRepository;
        $this->contestantRepository = $contestantRepository;
        $this->contestant = $contestant;
    }

    public function index()
    {
        $this->view_data['contestants'] = Contestant::limit(3)->orderBy('id', 'desc')->get();
        $this->view_data['events'] = $this->eventRepository->getUpcomingEvents();
        $this->view_data['latestEvent'] = Event::whereRaw('start_date > NOW()')
            ->orderBy('start_date', 'asc')->first();

        $this->view_data['winners'] = Contestant::where('is_winner', '=', true)->limit(3)->get();
        return view('frontend.index', $this->view_data);
    }

    public function slug($slug = null, Request $request)
    {
        $slug = $slug ? $slug : 'index';
        $file_path = resource_path() . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'frontend' . DIRECTORY_SEPARATOR . $slug . '.blade.php';
        if (file_exists($file_path)) {
            switch ($slug) {
                case 'index':
                    $this->view_data['contestants'] = Contestant::get()->limit(3);
                    $this->view_data['disciplines'] = $this->eventRepository->getAll();
                    break;
                case 'event':
                    $this->view_data['events'] = $this->eventRepository->getUpcomingEvents();
                    break;
                case 'contestant-apply':
                    $login = Auth::user();
                    if ($login == null)
                        return redirect()->route('contestant_signup')->withErrors('You have to login first to participate');
                    $this->view_data['events'] = $this->eventRepository->getAll();
                    break;
                case 'contestants':
                    $this->view_data['contestants'] = Contestant::orderBy('id', 'desc')->get();;
                    break;
                case 'winners':
                    $this->view_data['winners'] = Contestant::where('is_winner', '=', true)->get();
                    break;
                case 'exhibition':
                    $this->view_data['upcomingExhibition'] = Exhibition::whereRaw('start_date > NOW()')
                        ->orderBy('start_date', 'asc')->first();
                    $this->view_data['upcomingExhibitionImages'] = $this->view_data['upcomingExhibition']->exhibitionImages;
                    $this->view_data['upcomingMoreExhibition'] = Exhibition::whereRaw('start_date > NOW()')
                        ->orderBy('start_date', 'asc')->get();
                    break;



                default:
                    break;
            }
            return view('frontend.' . $slug, $this->view_data);
        }
        // 3. No page exist (404)
        return view('frontend.404');

    }

    public function registerContestant()
    {
        $this->view_data['events'] = $this->eventRepository->getAll();
        return view('frontend.signup', $this->view_data);
    }

    public function registerContestantStore(Request $request)
    {
        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'password_confirmation' => 'required|same:password'
        ]);

        $user = User::create(request(['name', 'email', 'password']));
        auth()->login($user);
        Session::flash('message','successfully Registered !!!');
        return redirect()->to('/contestant-signup');
    }

    public function loginContestant(Request $request)
    {
        $rules = array(
            'email' => 'required|email', // make sure the email is an actual email
            'password' => 'required' // password can only be alphanumeric and has to be greater than 3 characters
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return redirect('contestant-signup')
                ->withErrors($validator)// send back all errors to the login form
                ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
        } else {

            $userdata = array(
                'email' => Input::get('email'),
                'password' => Input::get('password')
            );

            if (Auth::attempt($userdata)) {

                Session::flash('message_login','successfully Logged In !!!');
                return redirect('event');

            } else {

                Session::flash('message_login','Credentials doesnot matched!!!');
                return redirect('contestant-signup');

            }


        }
    }

    public function vote(Request $request, $id)
    {
        $user = Auth::user();
        if ($user) {
            $votedUser = Vote::where('user_id', '=', $user->id)->where('event_id', '=', $id)->first();
            if (!$votedUser) {
                $contestant = Contestant::find($request->contestant_id);
                $contestant->vote = $contestant->vote + 1;
                $contestant->save();

                $vote = new Vote();
                $vote->user_id = $user->id;
                $vote->contestant_id = $request->contestant_id;
                $vote->event_id = $id;
                $vote->save();

                Session::flash('message', 'Successfully Voted!');
                Session::flash('alert-class', 'alert-success');

                return redirect()->back();
            }
            Session::flash('message', 'Thankyou!! You have already voted for this event');
            Session::flash('alert-class', 'alert-primary');
            return redirect()->back();
        }
        Session::flash('message', 'Please do Login First');
        Session::flash('alert-class', 'alert-danger');
        return redirect()->back();
    }

    public function eventDetail($slug)
    {
        $event = Event::where('slug', '=', $slug)->first();
        $contestants = Contestant::where('event_id', '=', $event->id)->get();
        return view('frontend.event-detail', compact('event', 'contestants'));
    }
    public function exhibitionDetail($id)
    {
        $exhibition = Exhibition::find($id);
        return view('frontend.exhibition-detail', compact('exhibition'));

    }

    public function sendFeedback(Request $request)
    {
        $loginUser = Auth::user();

        $feedback = new Feedback();
        $feedback->user_id = $loginUser->id;
        $feedback->exhibition_id = $request->exhibition_id;
        $feedback->feedback = $request->feedback;
        $feedback->save();

        Session::flash('message', 'Successfully Submitted Feedback!');
        Session::flash('alert-class', 'alert-success');

        return redirect('exhibition');

    }
}
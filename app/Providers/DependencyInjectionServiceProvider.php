<?php

namespace App\Providers;

use App\Modules\Backend\Contestant\Repositories\ContestantRepository;
use App\Modules\Backend\Contestant\Repositories\EloquentContestantRepository;
use App\Modules\Backend\Event\Repositories\EloquentEventRepository;
use App\Modules\Backend\Exhibition\Repositories\EloquentExhibitionRepository;
use App\Modules\Backend\Event\Repositories\EventRepository;
use App\Modules\Backend\Exhibition\Repositories\ExhibitionRepository;
use App\Modules\Backend\ExhibitionImage\Repositories\EloquentExhibitionImageRepository;
use App\Modules\Backend\ExhibitionImage\Repositories\ExhibitionImageRepository;
use Illuminate\Support\ServiceProvider;

class DependencyInjectionServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        /**
         * Event dependency
         */


        $this->app->bind(
            EventRepository::class,
            EloquentEventRepository::class
        );
        /**
         * Exhibition dependency
         */


        $this->app->bind(
            ExhibitionRepository::class,
            EloquentExhibitionRepository::class
        );
//        Contestant Depepndeny
        $this->app->bind(
            ContestantRepository::class,
            EloquentContestantRepository::class
        );
        //        Exhibition Image Depepndeny
        $this->app->bind(
            ExhibitionImageRepository::class,
            EloquentExhibitionImageRepository::class
        );


    }
}

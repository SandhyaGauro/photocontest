<?php
/**
 * Created by PhpStorm.
 * User: Saurav KC
 * Date: 3/10/2018
 * Time: 05:05 AM
 */

namespace App\Modules\Backend\Event\Repositories;

use App\Modules\Framework\Repository;

interface EventRepository extends  Repository
{
    /*
              *params integer limit
         * params integer offset
            * return collection
                */
    public function getData($limit, $offset = 0);

}


<?php
/**
 * Created by PhpStorm.
 * User: Saurav KC
 * Date: 3/10/2018
 * Time: 05:09 AM
 */

namespace App\Modules\Backend\Event\Repositories;

use App\Event;
use App\Modules\Backend\Event\Repositories\EventRepository;
use App\Modules\Framework\RepositoryImplementation;

class EloquentEventRepository extends RepositoryImplementation implements EventRepository
{
    protected $entity_name = "Event";

    /**
     * Gets model for operation.
     *
     * @return mixed
     */
    public function getModel()
    {
        return new Event();
    }

    public function getData($limit, $offset = 0)
    {
        return $this->getModel()
            ->where(function ($query) use ($limit, $offset) {

            })
            ->take($limit)
            ->get();

    }

    public function getUpcomingEvents()
    {
        return $this->getModel()->whereRaw('start_date > NOW()')
            ->orderBy('start_date', 'asc')->get();
    }

}


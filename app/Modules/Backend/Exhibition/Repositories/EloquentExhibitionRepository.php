<?php
/**
 * Created by PhpStorm.
 * User: Saurav KC
 * Date: 3/10/2018
 * Time: 05:09 AM
 */

namespace App\Modules\Backend\Exhibition\Repositories;

use App\Event;
use App\Exhibition;
use App\Modules\Backend\Event\Repositories\EventRepository;
use App\Modules\Backend\ExhibitionImage\Repositories\ExhibitionImageRepository;
use App\Modules\Framework\RepositoryImplementation;
use Psr\Log\LoggerInterface as Log;
use DB;

class EloquentExhibitionRepository extends RepositoryImplementation implements ExhibitionRepository
{
    protected $entity_name = "Event";
    private $exhibitionImageRepository;

    /**
     * Gets model for operation.
     *
     * @return mixed
     */
    public function __construct(Log $log,ExhibitionImageRepository $exhibitionImageRepository)
    {
        parent::__construct($log);
        $this->exhibitionImageRepository=$exhibitionImageRepository;
    }

    public function getModel()
    {
        return new Exhibition();
    }

    public function getData($limit, $offset = 0)
    {
        return $this->getModel()
            ->where(function ($query) use ($limit, $offset) {

            })
            ->take($limit)
            ->get();

    }

    public function getUpcomingEvents()
    {
        return $this->getModel()->whereRaw('start_date > NOW()')
            ->orderBy('start_date', 'asc')->get();
    }
    public function createWithDetails(array $data)
    {
        $images_data=$data['images_data'];
        DB::beginTransaction();
        try {
            $entity = $this->getModel()->create($data['data']);
            foreach ($images_data['image'] as $key=>$name){
                foreach($images_data  as $key2=>$s){
                    $data2[$key2]=$s[$key];
                }
                $data2['exhibition_id']=$entity->id;
                $schoolImages=$this->exhibitionImageRepository->create($data2);
            }

            DB::commit();
            return $entity;
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            $this->log->error($this->entity_name . ' create : ' . $e->getMessage());
            return false;
        }


    }
    public function updateWithDetails(array $data, $id)
    {
        $images_data=$data['images_data'];
        DB::beginTransaction();
        try {
            $entity = $this->update($data['data'], $id);

            if(isset($images_data['id']))
                $this->exhibitionImageRepository->getModel()->whereNotIn('id',$images_data['id'])->delete();
            foreach ($images_data['image'] as $key=>$image){
                foreach ($images_data as $key2 => $s) {
                    if(isset($s[$key]))
                        $data2[$key2] = $s[$key];

                }
                $data2['exhibition_id'] = $entity->id;
                if(isset($images_data['id'][$key])) {
                    $this->exhibitionImageRepository->update($data2,$images_data['id'][$key]);
                }else{
                    $this->exhibitionImageRepository->create($data2);
                }

            }
            DB::commit();
            return $entity;
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            $this->log->error($this->entity_name . ' create : ' . $e->getMessage());
            return false;
        }

    }

}


<?php
/**
 * Created by PhpStorm.
 * User: Saurav KC
 * Date: 3/10/2018
 * Time: 05:05 AM
 */

namespace App\Modules\Backend\Exhibition\Repositories;

use App\Modules\Framework\Repository;

interface ExhibitionRepository extends  Repository
{
    /*
              *params integer limit
         * params integer offset
            * return collection
                */
    public function getData($limit, $offset = 0);

    public function createWithDetails(array $data);
    public function updateWithDetails(array $data,$id);

}


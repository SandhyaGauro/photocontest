<?php
/**
 * Created by PhpStorm.
 * User: Saurav KC
 * Date: 3/10/2018
 * Time: 05:09 AM
 */

namespace App\Modules\Backend\Contestant\Repositories;

use App\Contestant;
use App\Event;
use App\Modules\Backend\Contestant\Repositories\ContestantRepository;
use App\Modules\Backend\Event\Repositories\EventRepository;
use App\Modules\Framework\RepositoryImplementation;

class EloquentContestantRepository extends RepositoryImplementation implements ContestantRepository
{
    protected $entity_name = "Contestant";

    /**
     * Gets model for operation.
     *
     * @return mixed
     */
    public function getModel()
    {
        return new Contestant();
    }

    public function getData($limit, $offset = 0)
    {
        return $this->getModel()
            ->where(function ($query) use ($limit, $offset) {

            })
            ->take($limit)
            ->get();

    }


}


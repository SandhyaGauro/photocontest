<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';
    protected $fillable = ['name', 'desc', 'image', 'slug', 'start_date'];

    public function getImage()
    {
        if (isset($this->image)) {
            return uploadedAsset('event', $this->image);
        } else {
            return imageNotFound();
        }
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contestant extends Model
{
    protected $table = 'contestants';
    protected $fillable = ['name', 'description', 'address', 'contact_no', 'image_type','image'];
    public function getImage()
    {
        if (isset($this->image)) {
            return uploadedAsset('contestant', $this->image);
        } else {
            return imageNotFound();
        }
    }


}

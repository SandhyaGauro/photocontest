<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExhibitionImage extends Model
{
    protected $table = 'exhibition_images';
    protected $fillable = [
        'exhibition_id',
        'image',
        'caption',
       ];
    public function getImage()
    {
        if (isset($this->image)) {
            return uploadedAsset('exhibitionimages', $this->image);
        } else {
            return imageNotFound();
        }
    }
}

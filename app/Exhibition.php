<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exhibition extends Model
{
    protected $table = 'exhibitions';
    protected $fillable = ['name', 'desc', 'image', 'slug', 'start_date'];
    public function getImage()
    {
        if (isset($this->image)) {
            return uploadedAsset('exhibition', $this->image);
        } else {
            return imageNotFound();
        }
    }
    public function exhibitionImages(){
           return $this->hasMany(ExhibitionImage::class,'exhibition_id','id');
    }


}

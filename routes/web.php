<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');
Route::get('/dashboard', 'Admin\DashboardController@index');

//Auth::routes();


// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register/success', 'Auth\RegisterController@success')->name('register.success');
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');


// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');


Route::resource('admin/exhibitions', 'Admin\ExhibitionController', [
    'names' => [
        'index' => 'admin.exhibitions.index',
        'create' => 'admin.exhibitions.create',
        'store' => 'admin.exhibitions.store',
        'show' => 'admin.exhibitions.show',
        'update' => 'admin.exhibitions.update',
        'edit' => 'admin.exhibitions.edit',
        'destroy' => 'admin.exhibitions.destroy',

    ]
]);

//Backend routes
Route::get('/admin/feedback', 'Admin\ExhibitionController@feedback')->name('admin.feedback');
Route::get('/admin/feedback/edit', 'Admin\ExhibitionController@editFeedback')->name('admin.feedback.edit');
Route::get('/admin/feedback/destroy', 'Admin\ExhibitionController@destroyFeedback')->name('admin.feedback.destroy');
Route::get('/admin/event', 'Admin\EventController@index')->name('event.index');
Route::get('/admin/event/{id}/show', 'Admin\EventController@show')->name('event.show');
Route::get('/admin/event/contestant/{id}', 'Admin\EventController@ajaxContestantEvent')->name('event.contestant.index');
Route::delete('/admin/event/contestant/winner-select/{id}', 'Admin\EventController@winnerSelect')->name('event.contestant.winner_select');
Route::delete('/admin/event/contestant/winner-reject/{id}', 'Admin\EventController@winnerReject')->name('event.contestant.winner_reject');
Route::get('/admin/event/create', 'Admin\EventController@create')->name('event.create');
Route::post('/admin/event/store', 'Admin\EventController@store')->name('event.store');
Route::get('/admin/event/edit/{eventId?}', 'Admin\EventController@edit')->name('event.edit');
Route::put('/admin/event/update/{eventId?}', 'Admin\EventController@update')->name('event.update');
Route::delete('/admin/event/destroy/{id}', 'Admin\EventController@destroy')->name('event.destroy');


Route::get('/admin/contestant', 'Admin\ContestantController@index')->name('contestant.index');
Route::get('/admin/contestant/create', 'Admin\ContestantController@create')->name('contestant.create');
Route::post('/admin/contestant/store', 'Admin\ContestantController@store')->name('contestant.store');
Route::get('/admin/contestant/edit/{contestantId?}', 'Admin\ContestantController@edit')->name('contestant.edit');
Route::put('/admin/contestant/update/{contestantId?}', 'Admin\ContestantController@update')->name('contestant.update');
Route::get('/admin/contestant/destroy', 'Admin\ContestantController@destroy')->name('contestant.destroy');


Route::group(['prefix' => 'dashboard/ajax', 'namespace' => 'Admin', 'middleware' => 'auth'], function () {
    Route::post('/file-upload/{id?}', 'BaseController@fileUpload')
        ->name('dashboard.ajax.file-upload'); //FOR CREATE
    Route::put('/file-upload/{id?}', 'BaseController@fileUpload')
        ->name('dashboard.ajax.file-upload'); //FOR UPDATE

    Route::post('/any-file-upload/{id?}', 'BaseController@anyFileUpload')
        ->name('dashboard.ajax.any-file-upload'); //FOR CREATE
    Route::put('/any-file-upload/{id?}', 'BaseController@anyFileUpload')
        ->name('dashboard.ajax.any-file-upload'); //FOR UPDATE
});


//frontend routes
Route::post('/feedback','PagesController@sendFeedback')->name('feedback');
Route::get('/event/{slug}','PagesController@eventDetail')->name('event.detail');
Route::get('/exhibition-detail/{id}','PagesController@exhibitionDetail')->name('exhibition.detail');
Route::post('/vote/{id?}','PagesController@vote')->name('vote');
Route::get('/contestant-signup','PagesController@registerContestant')->name('contestant_signup');
Route::post('/contestant-signup','PagesController@registerContestantStore');
Route::post('/contestant-login','PagesController@loginContestant');
Route::get('/contestant-apply/{id}','ContestantController@index');
Route::post('/contestant-apply/{id}','ContestantController@applyContestant')->name('contestant.apply');
Route::post('/contestant-apply','ContestantController@applyContestant2')->name('contestant.apply2');
Route::match(['get', 'post'], '/{slug}', 'PagesController@slug')->where('slug', '.*');

//Route::get('/home', 'HomeController@index')->name('index');
//Route::get('/contest', 'PagesController@contest')->name('contest');
//Route::get('/exhibition', 'PagesController@exhibition')->name('exhibition');
//Route::get('/winner', 'PagesController@winner')->name('winner');
//Route::get('/dash', 'PagesController@dash')->name('dash');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContestantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contestants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedBigInteger('event_id');
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
            $table->string('name')->nullable()->default('');
            $table->text('description')->nullable();
            $table->string('address')->nullable()->default('');
            $table->string('contact_no')->nullable()->default('');
            $table->integer('vote')->nullable();
            $table->boolean('is_winner')->nullable()->default(0);
            $table->enum('type',['event','exhibition'])->nullable();
            $table->enum('image_type',['landscape','portrait','culture','nature'])->nullable();
            $table->string('image')->nullable()->default('');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contestants');
    }
}

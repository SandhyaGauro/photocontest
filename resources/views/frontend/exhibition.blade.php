@extends('frontend.inc.layout')
@section('content')
    <div role="main" class="main">
        @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        <section class="section bg-light-5"  data-plugin-image-background
                 data-plugin-options="{'imageUrl': '{{asset($upcomingExhibition->getImage())}}'}">
            <div class="container pb-lg-1 mb-lg-4">
                <div class="row text-center mb-5">
                    <div class="col">
                        <h2 class="font-weight-bold" style="color: #fff;">{{isset($upcomingExhibition->name)?$upcomingExhibition->name:''}}</h2>
                        <p class="lead" style="color: #fff;">{{isset($upcomingExhibition->desc)?$upcomingExhibition->desc:''}}</p>
                    </div>
                </div>

            </div>
        </section>

        <div class="section section-content-pull-top pull-top-level-2">
            <div class="container">
                <div class="row mx-0">
                    <div class="owl-carousel owl-theme dots-style-3 nav-style-3 mb-0"
                         data-plugin-options="{'responsive': {'0': {'items': 1}, '576': {'items': 1}, '768': {'items': 2}, '979': {'items': 2}, '1199': {'items': 2}}, 'dots': true, 'nav': true, 'loop': false, 'margin': 10}">
                        @foreach($upcomingExhibitionImages as $upcomingImage)
                        <div>
                            <div class="image-frame">
                                <img src="{{url($upcomingImage->getImage())}}"  style="height: 400px;" class="img-fluid" alt=""/>
                            </div>
                        </div>
                        @endforeach

{{--                        <div>--}}
{{--                            <div class="image-frame">--}}
{{--                                <img src="assets/img/projects/photos/project-2-square.jpg" class="img-fluid" alt=""/>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div>--}}
{{--                            <div class="image-frame">--}}
{{--                                <img src="assets/img/projects/generic/project-3-square.jpg" class="img-fluid" alt=""/>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div>--}}
{{--                            <div class="image-frame">--}}
{{--                                <img src="assets/img/projects/photos/project-3-square.jpg" class="img-fluid" alt=""/>--}}
{{--                            </div>--}}
{{--                        </div>--}}


                    </div>
                </div>

            </div>
        </div>
        <section class="section section-content-pull-top-2 pull-top-level-3 z-index-1 py-lg-0 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="400">
            <div class="container pb-0">
                <div class="row justify-content-center">
                    <div class=" d-none d-md-block appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1400"></div>
                    <div class="col-md-5 col-lg-4 p-md-4 p-lg-5 mb-5 mb-md-4 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="1600">

                    </div>
                </div>
            </div>
            <div class="container no-pull-top">
                <div class="row text-center">
                    <div class="col appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1800" data-plugin-options="{'accY': 100}">
                        <a class="btn btn-primary btn-rounded btn-v-3 btn-h-3 font-weight-bold text-0" href="#collapseForm" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseForm" onClick="scrollAndFocus('#form', '#name', 30);">SEND FEEDBACK</a>
                    </div>
                </div>
            </div>
        </section>
        <div class="section bg-light-5 mt-negative-2 z-index-0 py-0">
            <div class="container">
                <div id="form" class="row">
                    <div class="col">
                        <div class="collapse pr-4" id="collapseForm">
        @if(Auth::check())
                            <form class="contact-form pt-5 mt-5" action="{{route('feedback')}}" method="POST">
                                {!! csrf_field() !!}

                                <div class="contact-form-success alert alert-success d-none">
                                    <strong>Success!</strong> Your message has been sent to us.
                                </div>
                                <div class="contact-form-error alert alert-danger d-none">
                                    <strong>Error!</strong> There was an error sending your message.
                                    <span class="mail-error-message d-block"></span>
                                </div>
                                <div class="form-row mb-4">
                                    <div class="form-group col">
                                        <input type="hidden" name="exhibition_id" value="{{$upcomingExhibition->id}}">
                                        <textarea maxlength="5000"  rows="5" class="form-control" name="feedback" id="message" placeholder="Send feedback" required></textarea>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col">
                                        <input type="submit" value="SEND FEEDBACK" class="btn btn-primary btn-rounded btn-4 font-weight-semibold text-0" data-loading-text="Loading...">
                                    </div>
                                </div>
                            </form>
                            @else
                                <div class="contact-form-error alert alert-danger ">
                                    <strong>Please Login!</strong> to write a feedback for Exhibition
                                    <span class="mail-error-message d-block"></span>
                                </div>
                            @endif

                        </div>
                    </div>
                </div>

            </div>
        </div>


        <hr class="my-0">

        <section class="section bg-light-5">
            <div class="container">
               <h2 style="text-align: center; margin-bottom: 20px"> More Exhibition to Come!!</h2>
                <div class="row justify-content-center">

@foreach($upcomingMoreExhibition as $exhibition)
                    <div class="col-md-6 col-lg-4 mb-5 mb-lg-0">
                        <div class="card rounded bg-light border-0"  style="margin-bottom: 20px;">
{{--                            <a href="portfolio-detail-2.html">--}}
                                <img src="{{$exhibition->getImage()}}"
                                     class="card-img-top hover-effect-2"
                                     alt=""
                                style="height: 250px;">
{{--                            </a>--}}
                            <div class="card-body">
                                <h2 class="font-weight-bold text-4 mb-0">
                                    <a href="{{ url('/exhibition-detail',$exhibition->id) }}" class="link-color-dark">
                                        {{$exhibition->name}}
                                    </a>
                                </h2>

                                <p class="text-color-light-3 mb-0">{{$exhibition->desc}}</p>
                            </div>
                        </div>
                    </div><br>
@endforeach

                </div>
            </div>
        </section>

    </div>

@endsection


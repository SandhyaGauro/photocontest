@extends('frontend.inc.layout')
@section('content')
    <style>
        div.scroll-view {
            height: 100px;
            overflow: auto;
        }
    </style>
    <div role="main" class="main">
        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1>Contestant</h1>
                        <p class="lead">Vote on image </p>
                    </div>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="container">
                @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                @endif
                <div class="row masonry-loader masonry-loader-showing portfolio-list portfolio-list-style-2"
                     data-plugin-masonry data-plugin-options="{'itemSelector': '.isotope-item'}">
                    @foreach($contestants as $c)
                        <div class="col-sm-6 col-md-4 isotope-item mb-5 p-0">
                            <div class="portfolio-item">
                                <article class="blog-post">
                                    {{--<span class="top-sub-title text-color-primary">Jan 15, 2018</span>--}}
                                    <h2 class="font-weight-bold text-4 mb-3">
                                        {{$c['name']}}
                                    </h2>
                                    <div class="image-frame hover-effect-2">
                                        <div class="image-frame-wrapper">
                                          <img
                                                        src="{{asset($c->getImage())}}"
                                                        class="img-fluid" alt=""
                                                        style="height: 210px;width: auto;"/>
                                        </div>
                                    </div>
                                    <div class="d-flex opacity-6 my-2">
                                        <form action="{{route('vote',$c->event_id)}}" method="POST"
                                              enctype="multipart/form-data">
                                            {!! csrf_field() !!}
                                            <input type="hidden" value="{{ $c->id }} " name="contestant_id">
                                            <button type="submit" class="btn btn-primary">
                                                <b> Vote</b>
                                            </button>
                                        </form>
                                        <span class="post-likes d-flex align-items-center border border-grey border-top-0  border-bottom-0 border-left-0 pl-3 pr-3">  <p style=" font-weight: 600;
    font-size: 15px;">  {{$c->vote ? $c->vote : 0}} votes</p></span>

                                    </div>

                                    <hr class="mt-0 mb-3">
                                    <p class="text-color-light-3">
                                        <div class="scroll-view">
                                        {{$c['description']}}
                                    </div>
                                    </p>

                                </article>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    </div>
@endsection
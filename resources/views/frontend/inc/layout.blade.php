@include('frontend.inc.header')

<div class="body">
    <header id="header" class="header-effect-shrink"
            data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 120}">
        <div class="header-body">
{{--            <div class="header-top">--}}
{{--                <div class="header-top-container container">--}}
{{--                    <div class="header-row">--}}
{{--                        <div class="header-column justify-content-start">--}}
{{--									<span class="d-none d-sm-flex align-items-center">--}}
{{--										<i class="fas fa-map-marker-alt mr-1"></i>--}}
{{--										Kathmandu,Nepal--}}
{{--									</span>--}}
{{--                            <span class="d-none d-sm-flex align-items-center ml-4">--}}
{{--										<i class="fas fa-phone mr-1"></i>--}}
{{--										<a href="tel:+1234567890">123-456-7890</a>--}}
{{--									</span>--}}
{{--                        </div>--}}
{{--                        <div class="header-column justify-content-end">--}}
{{--                            <ul class="nav">--}}
{{--                                <li class="nav-item">--}}
{{--                                    <a class="nav-link" href="contact.html">Contact Us</a>--}}
{{--                                </li>--}}
{{--                                <!-- <li class="nav-item">--}}
{{--                                    <a href="#" class="nav-link dropdown-menu-toggle py-2" id="dropdownLanguage" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">--}}
{{--                                        English	<i class="fas fa-angle-down fa-sm"></i>--}}
{{--                                    </a>--}}
{{--                                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownLanguage">--}}
{{--                                        <li><a href="#" class="no-skin"><img src="img/blank.gif" class="flag flag-us" alt="English" /> English</a></li>--}}
{{--                                        <li><a href="#" class="no-skin"><img src="img/blank.gif" class="flag flag-es" alt="Español" /> Español</a></li>--}}
{{--                                        <li><a href="#" class="no-skin"><img src="img/blank.gif" class="flag flag-fr" alt="Française" /> Française</a></li>--}}
{{--                                    </ul>--}}
{{--                                </li> -->--}}
{{--                            </ul>--}}
{{--                            <ul class="header-top-social-icons social-icons social-icons-transparent d-none d-md-block">--}}
{{--                                <li class="social-icons-facebook">--}}
{{--                                    <a href="http://www.facebook.com/" target="_blank" title="Facebook"><i--}}
{{--                                                class="fab fa-facebook-f"></i></a>--}}
{{--                                </li>--}}
{{--                                <li class="social-icons-twitter">--}}
{{--                                    <a href="http://www.twitter.com/" target="_blank" title="Twitter"><i--}}
{{--                                                class="fab fa-twitter"></i></a>--}}
{{--                                </li>--}}
{{--                                <li class="social-icons-instagram">--}}
{{--                                    <a href="http://www.instagram.com/" target="_blank" title="Instragram"><i--}}
{{--                                                class="fab fa-instagram"></i></a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}

{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
            <div class="header-container container">
                <div class="header-row">
                    <div class="header-column justify-content-start">
                        <div class="header-logo">
                            <a href="{{url('/')}}">
                                <img alt="EZ" width="127" src="{{asset('assets/img/Contestlogo.png')}}">
                            </a>
                        </div>
                    </div>
                    <div class="header-column justify-content-end">
                        <div class="header-nav">
                            <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
                                <nav class="collapse">
                                    <ul class="nav flex-column flex-lg-row" id="mainNav">
                                        <li class="dropdown dropdown-mega  {{ Request::segment(1) === 'event' ? 'active' : null }}">
                                            <a class="dropdown-item dropdown-toggle  {{ Request::segment(1) === 'event' ? 'active' : null }}" href="{{url('/event')}}">
                                                Event
                                            </a>

                                        </li>
                                        <li class="dropdown dropdown-mega {{ Request::segment(1) === 'contestant-apply' ? 'active' : null }}">
                                            <a class="dropdown-item dropdown-toggle  {{ Request::segment(1) === 'contestant-apply' ? 'active' : null }} "
                                               href="{{url('contestant-apply')}}">
                                                Apply
                                            </a>

                                        </li>

                                        <li class="dropdown dropdown-mega {{ Request::segment(1) === 'contestants' ? 'active' : null }} ">
                                            <a class="dropdown-item dropdown-toggle {{ Request::segment(1) === 'contestants' ? 'active' : null }} " href="{{url('contestants')}}">
                                                Contestants
                                            </a>

                                        </li>
                                        <li class="dropdown dropdown-mega dropdown-mega-style-2 {{ Request::segment(1) === 'winners' ? 'active' : null }} ">
                                            <a class="dropdown-item dropdown-toggle {{ Request::segment(1) === 'winners' ? 'active' : null }} " href="{{url('winners')}}">
                                                Winners
                                            </a>

                                        </li>
                                        <li class="dropdown dropdown-mega dropdown-mega-style-2 {{ Request::segment(1) === 'exhibition' ? 'active' : null }} ">
                                            <a class="dropdown-item dropdown-toggle {{ Request::segment(1) === 'exhibition' ? 'active' : null }} " href="{{url('exhibition')}}">
                                                Exhibition
                                            </a>

                                        </li>


                                    </ul>

                                </nav>
                            </div>

                        </div>
                    </div>
                    <div>
                        @if (Auth::check())
                            <form action="{{ route('logout') }}" method="POST">
                                {{ csrf_field() }}
                                <button class="btn  btn-primary"
                                        type="submit">
                                    Logout
                                </button>

                            </form>
                        @else

                                <a class="btn  btn-primary "
                                   href="{{url('/contestant-signup')}}">
                                    Login / Sign up
                                </a>

                        @endif
                    </div>
                </div>
            </div>
        </div>
    </header>


    @yield('content')

    @include('frontend.inc.footer')
</div>

@extends('frontend.inc.layout')
@section('content')

    <div role="main" class="main">
        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1>Apply for event</h1>
                        <p class="lead">Application Form </p>
                    </div>
                </div>
            </div>
        </section>
        <section class="section">
            <div class="container">
                <div class="row text-center">
                    <div class="col">
                        <span class="top-sub-title text-color-primary appear-animation"
                              data-appear-animation="fadeInUpShorter"></span>
                        <h2 class="font-weight-bold appear-animation" data-appear-animation="fadeInUpShorter">
                            Application for Contestant
                        </h2>
                        @if(session()->has('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="row pt-5">
                    <div class="col-lg-2">
                        <div class="row">

                        </div>
                    </div>
                    <div class="col-lg-8 appear-animation" data-appear-animation="fadeInRightShorter">
                        @if(isset($id))
                            <form class="contact-form form-style-2" action="{{route('contestant.apply',$id)}}"
                                  method="POST" enctype="multipart/form-data">
                                @else
                                    <form class="contact-form form-style-2" action="{{route('contestant.apply2')}}"
                                          method="POST" enctype="multipart/form-data">
                                        @endif

                                        {!! csrf_field() !!}
                                        <div class="contact-form-success alert alert-success d-none">
                                            <strong>Success!</strong> Your message has been sent to us.
                                        </div>
                                        <div class="contact-form-error alert alert-danger d-none">
                                            <strong>Error!</strong> There was an error sending your message.
                                            <span class="mail-error-message d-block"></span>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <input type="text" value="" data-msg-required="Please enter your name."
                                                       maxlength="100" class="form-control" name="name" id="name"
                                                       placeholder="Name"
                                                       required>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <input type="text" value=""
                                                       data-msg-required="Please enter your address."
                                                       data-msg-email="Please enter a valid email address."
                                                       maxlength="100"
                                                       class="form-control" name="address" placeholder="Address">
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <input type="number" value=""
                                                       data-msg-required="Please enter your contact number."
                                                       maxlength="100" class="form-control" name="contact_no"
                                                       placeholder="Contact number" required>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <select class="form-control" name="image_type">
                                                    <option value="landscape">Landscape</option>
                                                    <option value="portrait">Portrait</option>
                                                    <option value="culture">Culture</option>
                                                    <option value="nature">Nature</option>
                                                </select>

                                            </div>
                                        </div>
                                        <div class="form-row">
                                            @if(!isset($id))
                                                <div class="form-group col-md-6">
                                                    <select class="form-control" name="event_id">
                                                        @foreach($events as $event)
                                                            <option value="{{$event->id}}">{{$event->name}}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            @endif
                                            <div class="form-group col-md-6">
                                                <input type="file" name="image" class="form-control" required>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col">
                                    <textarea maxlength="5000" data-msg-required="Please enter your description."
                                              rows="5" class="form-control" name="description" placeholder="Description"
                                              required></textarea>
                                            </div>
                                        </div>
                                        <div class="form-row mt-2">
                                            <div class="col">
                                                <input type="submit" value="APPLY"
                                                       class="btn btn-primary btn-rounded btn-4 font-weight-semibold text-0"
                                                       data-loading-text="Loading...">
                                            </div>
                                        </div>
                                    </form>
                    </div>
                </div>
            </div>
        </section>

    </div>

@endsection
@extends('frontend.inc.layout')
@section('content')


    <div role="main" class="main">
        <section class="page-header mb-0">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="breadcrumb">
                            <li><a href="index.html">Home</a></li>
                            <li class="active">Pages</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="font-weight-bold">Login / Register</h1>

                    </div>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="container">
                @if(!empty($errors->first()))
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        {{ $errors->first() }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                    @if(Session::has('message'))
                        <p class="alert alert-info">{{ Session::get('message') }}</p>
                    @endif
                <div class="row">
                    <div class="col-lg-6 mb-5 mb-lg-0 appear-animation" data-appear-animation="fadeInRightShorter">
                        <div class="bg-primary rounded p-5">
                            <span class="top-sub-title text-color-light-2">ALREADY A MEMBER?</span>
                            <h2 class="text-color-light font-weight-bold text-4 mb-4">Sign In</h2>
                            @if(Session::has('message_login'))
                                <p class="alert alert-info">{{ Session::get('message_login') }}</p>
                            @endif
                            <form action="{{url('contestant-login')}}" method="post">
                                {!! csrf_field() !!}
                                <div class="form-row">
                                    <div class="form-group col mb-2">
                                        <label class="text-color-light-2" for="frmSignInEmail">EMAIL </label>
                                        <input type="email" value="" maxlength="100" class="form-control bg-light rounded border-0 text-1" name="email" id="frmSignInEmail" required>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col">
                                        <label class="text-color-light-2" for="frmSignInPassword">PASSWORD</label>
                                        <input type="password" value="" class="form-control bg-light rounded border-0 text-1" name="password" id="frmSignInPassword" required>
                                    </div>
                                </div>
                                <div class="form-row mb-3">
                                    <div class="form-group col">
                                        <div class="form-check checkbox-custom checkbox-custom-transparent checkbox-default">
                                            <input class="form-check-input" type="checkbox" id="frmSignInRemember">
                                            <label class="form-check-label text-color-light-2" for="frmSignInRemember">
                                                Remember me
                                            </label>
                                        </div>
                                    </div>
{{--                                    <div class="form-group col text-right">--}}
{{--                                        <a href="#" class="forgot-pw text-color-light-2 d-block">Forgot password?</a>--}}
{{--                                    </div>--}}
                                </div>
                                <div class="row align-items-center">
                                    <div class="col text-right">
                                        <button type="submit" class="btn btn-dark btn-rounded btn-v-3 btn-h-3 font-weight-bold">SIGN IN</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-6 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="200">
                        <div class="border rounded h-100 p-5">
                            <span class="top-sub-title text-color-primary">DON'T HAVE AN ACCOUNT?</span>
                            <h2 class="font-weight-bold text-4 mb-4">Register Now!</h2>


                            <form action="{{url('contestant-signup')}}" id="frmRegister" method="post">
                                {!! csrf_field() !!}
                                <div class="form-row">
                                    <div class="form-group col mb-2">
                                        <label for="name">Name </label>
                                        <input type="text" value="{{ old('name') }}" maxlength="100" class="form-control bg-light-5 rounded border-0 text-1" name="name" id="name" required>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col mb-2">
                                        <label for="frmRegisterEmail">EMAIL </label>
                                        <input type="email" value="{{ old('email') }}" maxlength="100" class="form-control bg-light-5 rounded border-0 text-1" name="email" required>
                                    </div>
                                </div>
                                <div class="form-row mb-5">
                                    <div class="form-group col-lg-6">
                                        <label >PASSWORD</label>
                                        <input type="password" value="{{ old('password') }}" class="form-control bg-light-5 rounded border-0 text-1" name="password" required>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label >RE-ENTER PASSWORD</label>
                                        <input type="password" value="" class="form-control bg-light-5 rounded border-0 text-1" name="password_confirmation" required>
                                    </div>
                                </div>
                                <div class="row align-items-center">
                                    <div class="col text-right">
                                        <button type="submit" class="btn btn-primary btn-rounded btn-v-3 btn-h-3 font-weight-bold">REGISTER NOW</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
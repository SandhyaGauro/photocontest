@extends('frontend.inc.layout')
@section('content')

    <div role="main" class="main">
        @if(Session::has('message_login'))
            <p class="alert alert-info">{{ Session::get('message_login') }}</p>
        @endif
        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1>Upcoming Events</h1>
                        <p class="lead">Events </p>
                    </div>
                </div>
            </div>
        </section>
        <div class="container mb-5 pb-3">
            <div class="row mb-5">
                @foreach($events as $event)
                    <div class="col-md-3 mb-5 mb-md-0 text-center">
                        <h5 class="mb-4">{{$event->name}}</h5>
                        <div class="image-frame image-frame-border image-frame-style-1 image-frame-effect-2 image-frame-effect-1">
                            <div class="image-frame-wrapper image-frame-wrapper-overlay-bottom image-frame-wrapper-overlay-light image-frame-wrapper-align-end">
                                <img src="{{asset($event->getImage())}}" style="height: 240px;" class="img-fluid" alt="">
                                <div class="image-frame-action">
                                    <a href="{{url('contestant-apply',$event->id)}}"
                                       class="btn btn-primary btn-rounded font-weight-semibold btn-v-3 btn-fs-2">Participate</a>
{{--                                    <a href="#" data-toggle="modal" data-target="#myModal"--}}
{{--                                       class="btn btn-primary btn-rounded font-weight-semibold btn-v-3 btn-fs-2">View Image</a>--}}

                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach


            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>

                </div>
                <div class="modal-body">


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endsection
@extends('frontend.inc.layout')
@section('content')

    <div role="main" class="main">
        @if($latestEvent != null)
            <section class="section section-background section-height-3" data-plugin-image-background
                     data-plugin-options="{'imageUrl': '{{asset($latestEvent->getImage())}}'}">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h1 style="color: #fff;">Upcoming Event "{{$latestEvent->name}}"</h1>
                            <p style="color: #fff;" class="lead">You can vote for pictures you like.</p>
                        </div>
                    </div>
                </div>
            </section>
        @endif
        <div class="container mb-5 pb-3">

            <div class="row mt-5">
                @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                @endif
                <div class="col-md-12">
                    <h4 class="mb-4">Events
                        <button type="button" class="btn btn-primary mb-2"><a href="{{url('event')}}"
                                                                              style="color: #fff;"> View All</a>
                        </button>
                    </h4>

                    <div class="owl-carousel owl-theme nav-style-3"
                         data-plugin-options="{'responsive': {'0': {'items': 1}, '479': {'items': 1}, '768': {'items': 2}, '979': {'items': 3}, '1199': {'items': 4}}, 'dots': false, 'nav': true, 'animateIn': 'fadeIn', 'animateOut': 'fadeOut', 'margin': 30}">
                        @foreach($events as $event)

                            <div>
                                <a href="{{url('event/'.$event->slug)}}">
                                    <img src="{{asset($event->getImage())}}" style="    height: 174px;" class="img-fluid" alt="">
                                </a>
                            </div>
                        @endforeach

                    </div>

                </div>

            </div>
            <div class="row mt-5">

                <section class="section border boder-left-0 border-right-0">
                    <div class="container">
                        <h4 class="mb-4">Contestants
                            <button type="button" class="btn btn-primary mb-2"><a href="{{url('contestants')}}"
                                                                                  style="color: #fff;"> View All</a>
                            </button>
                        </h4>
                        <div class="row appear-animation" data-appear-animation="fadeInLeftShorter">
                            @foreach($contestants as $contestant)
                                <div class="col-md-4 text-center mb-5 mb-md-0">
                                    <div>
                                        <img src="{{asset($contestant->getImage())}}" style="width: 820px;    height: 175px;
   " class="img-fluid" alt="">
                                    </div>
                                    <h2 class="text-4 font-weight-bold mb-3">{{$contestant->name}}
                                    </h2 >
                                   <p style="color: #1386a9;
    font-weight: 800;
    font-size: 15px;"> {{$contestant->vote ? $contestant->vote : 0}} votes</p>
{{--                                        <form action="{{route('vote',$contestant->event_id)}}" method="POST"--}}
{{--                                              enctype="multipart/form-data">--}}
{{--                                            {!! csrf_field() !!}--}}
{{--                                            <button type="submit" class="btn btn-primary">--}}
{{--                                        <span class="post-likes d-flex align-items-center border border-grey border-top-0  border-bottom-0 border-left-0 pl-3 pr-3">  {{$contestant->vote ? $contestant->vote : 0}} votes</span>--}}
{{--                                            </button>--}}
{{--                                        </form>--}}

{{--                                    <button class="btn btn-primary">--}}
{{--                                        <span class="post-likes d-flex align-items-center border border-grey border-top-0  border-bottom-0 border-left-0 pl-3 pr-3">--}}
{{--                                            {{$contestant->vote ? $contestant->vote : 0}} votes--}}
{{--                                        </span>--}}
{{--                                    </button>--}}


                                    <p>{{$contestant->description}}</p>
                                    {{--<a href="#"--}}
                                    {{--class="btn btn-link font-weight-semibold text-decoration-none align-items-center text-0 d-inline-flex">VIEW--}}
                                    {{--MORE <i class="fas fa-angle-right text-3 ml-3"></i></a>--}}
                                </div>
                            @endforeach

                        </div>
                    </div>
                </section>
            </div>
            <div class="row mt-5">

                <div class="container mb-5 pb-3">

                    <div class="row">

                        <div class="col">
                            <h4>Winners <a href="{{url('winners')}}" class="btn btn-primary mb-2">View all</a>
                            </h4>
                            <hr class="mb-5">

                        </div>

                    </div>

                    <div class="row align-items-center mb-4">
                        <div class="col-12 col-md-8 col-lg-9">
                            <ul id="portfolioLoadMoreFilter"
                                class="nav sort-source justify-content-center justify-content-md-start mb-4 mb-md-0"
                                data-sort-id="portfolio" data-option-key="filter"
                                data-plugin-options="{'layoutMode': 'fitRows', 'filter': '*'}">

                            </ul>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-12 pl-3">
                            <div class="sort-destination-loader sort-destination-loader-showing">
                                <ul id="portfolioLoadMoreWrapper" class="portfolio-list sort-destination"
                                    data-sort-id="portfolio" data-total-pages="3">
                                    @foreach($winners as $winner)
                                        <li class="col-12 col-md-6 col-lg-4 p-0 isotope-item brands">
                                            <div class="portfolio-item hover-effect-3d appear-animation"
                                                 data-appear-animation="fadeInUpShorter"
                                                 data-plugin-options="{'accY' : -50}">
                                                {{--<a href="portfolio-detail.html">--}}
                                                <span class="image-frame image-frame-style-1 image-frame-effect-1">
													<span class="image-frame-wrapper">
														<img src="{{asset($winner->getImage())}}"
                                                             class="img-fluid" alt="" style="    height: 250px;">
														<span class="image-frame-inner-border"></span>

													</span>
												</span>
                                                {{--</a>--}}
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </div>


    </div>
@endsection
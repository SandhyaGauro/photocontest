@extends('frontend.inc.layout')
@section('content')
    <div role="main" class="main">
        @if($exhibition != null)
        <div class="container">
            <div class="row mt-4">
                <div class="col p-0">

                    <div class="slider-container slider-container-height-490 rev_slider_wrapper">
                        <div id="revolutionSlider" class="slider rev_slider" data-version="5.4.7" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'sliderLayout': 'auto', 'gridwidth': [1140,960,720,540], 'gridheight': [490,490,490,490], 'disableProgressBar': 'on', 'responsiveLevels': [4096,1200,992,576], 'navigation' : {'arrows': { 'enable': true, 'style': 'slider-arrows-style-1 slider-arrows-dark' }, 'bullets': {'enable': true, 'style': 'bullets-style-1', 'h_align': 'right', 'v_align': 'bottom', 'space': 7, 'v_offset': 15, 'h_offset': 15}}}">

                                    <img style="width: 100%;" src="{{ asset($exhibition->getImage()) }}"
                                         alt=""
                                         data-bgposition="center center"
                                         data-bgfit="cover"
                                         data-bgrepeat="no-repeat"
                                         class="rev-slidebg">
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <section class="section bg-light pb-5 mb-1">
            <div class="container">
                <div class="row align-items-baseline">
                    <div class="col-sm-12 col-lg-12 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="200">
                        <div class="icon-box icon-box-style-3 text-center">
                            <div class="icon-box-icon">
                                <img width="42" height="42" src="vendor/linear-icons/tablet.svg" alt="" data-icon data-plugin-options="{'color': '#2388ED', 'animated': true, 'delay': 400}" />
                            </div>
                            <div class="icon-box-info">
                                <div class="icon-box-info-title">
                                    <h2 class="font-weight-bold text-4 mb-3"> {{strtoupper($exhibition->name)}}</h2>
                                </div>
                                <p> {{$exhibition->desc}}</p>
                            </div>
                        </div>
                    </div>
                      </div>
            </div>
        </section>


        @endif



            <section class="section pt-5 pb-3">
            <div class="container">
                <div class="row text-center mb-4">
                    <div class="col">

                        <div class="overflow-hidden mb-2">
                            <h2 class="font-weight-bold mb-0 appear-animation" data-appear-animation="maskUp"
                                data-appear-animation-delay="200">More Images</h2>
                        </div>
                    </div>
                </div>
                <div class="row appear-animation" data-appear-animation="fadeInUpShorter"
                     data-appear-animation-delay="400">
                    @foreach($exhibition->exhibitionImages()->get() as $image)
                        <div class="col-md-3">
                            <div class="product mb-4">
                                <div class="image-frame image-frame-style-1 image-frame-effect-2 mb-3">
                                    <img style="height : 225px;width: 225px;" src="{{ url($image->getImage()) }}" class="img-fluid"
                                         alt="">
                                </div>

                            </div>
                        </div>

                    @endforeach

                </div>
            </div>
        </section>

    </div>

@endsection
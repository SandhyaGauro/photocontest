<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta charset="utf-8"/>
    <title> {{ config('app.site_name') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta content="" name="description"/>
    <meta content="Saurav Kc" name="author"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('backend.layout.style')
    @stack('styles')
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="">
@include('backend.layout.header')
<!-- BEGIN CONTAINER -->
<div class="page-container row-fluid">
@include('backend.sidebar.default')

<!-- BEGIN PAGE CONTAINER-->
    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div class="content">
            @yield('content')
        </div>
    </div>
</div>
<!-- END CONTAINER -->
@include('backend.layout.script')
@include('backend.layout.notification')
@stack('scripts')
</body>
</html>
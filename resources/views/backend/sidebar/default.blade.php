<!-- BEGIN SIDEBAR -->
<div class="page-sidebar " id="main-menu">
    <!-- BEGIN MINI-PROFILE -->
    <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper">
        <div class="user-info-wrapper sm">
            <div class="profile-wrapper sm">
                <img src="{{asset('/assets/img/profiles/avatar.jpg')}}" alt="" data-src="{{asset('assets/img/profiles/avatar.jpg')}}" data-src-retina="{{asset('assets/img/profiles/avatar2x.jpg')}}" width="69" height="69" />
                <div class="availability-bubble online"></div>
            </div>
            <div class="user-info sm">
                <div class="username"><span class="semi-bold">{{Auth::user()->name}}</span></div>
                <div class="status">{{Auth::user()->email}}</div>
            </div>
        </div>
        <!-- END MINI-PROFILE -->
        <!-- BEGIN SIDEBAR MENU -->

        <div class="side-bar-widgets">
            <p class="menu-title sm">INFORMATION <span class="pull-right"><a href="#" class="create-folder"> <i class="material-icons">add</i></a></span></p>
            <ul class="folders">
                <li>
                    <a href="{{url('dashboard')}}">
                        <div class="status-icon green"></div>
                        Dashboard </a>
                </li>
                <li>
                    <a href="{{route('event.index')}}">
                        <div class="status-icon green"></div>
                       Events Management </a>
                </li>
                <li>
                    <a href="{{route('admin.exhibitions.index')}}">
                        <div class="status-icon green"></div>
                        Exhiition Management </a>
                </li>
                <li>
                    <a href="{{route('admin.feedback')}}">
                        <div class="status-icon green"></div>
                        Exhiition Feedback </a>
                </li>

                {{--<li>--}}
                    {{--<a href="#">--}}
                        {{--<div class="status-icon red"></div>--}}
                        {{--To do list </a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="#">--}}
                        {{--<div class="status-icon blue"></div>--}}
                        {{--Projects </a>--}}
                {{--</li>--}}
                {{--<li class="folder-input" style="display:none">--}}
                    {{--<input type="text" placeholder="Name of folder" class="no-boarder folder-name" name="">--}}
                {{--</li>--}}
            </ul>

        </div>
        <div class="clearfix"></div>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<a href="#" class="scrollup">Scroll</a>
<div class="footer-widget">

    <div class="pull-right">
{{--        <div class="details-status"> </div>--}}
        </div>
</div>
<!-- END SIDEBAR -->
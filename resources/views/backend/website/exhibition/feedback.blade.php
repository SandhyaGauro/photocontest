@extends('backend.layout.app')

@section('content')
    @include('backend.partials.common.page-title', ['page_title' => ' Exhibition Management'])


    <div>
        <div class="row-fluid">
            <div class="span12">
                <div class="grid simple ">
                    <div class="grid-title">
                        {{--<a href="{{route('admin.exhibitions.create')}}"  class="btn btn-info btn-cons">--}}
                            {{--<i class="fa fa-plus-square"></i> Add New--}}
                        {{--</a>--}}
                        Feedback Lists
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="grid-body ">
                        <table class="table table-hover table-condensed" id="data-table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Exhibition</th>
                                <th>Written by</th>
                                <th>Feedback</th>

                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $('#data-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin.feedback') }}',
            columns: [
                {data: 'id', name: 'id'},
                {data: 'exhibition_id', name: 'exhibition_id'},
                {data: 'user_id', name: 'user_id'},
//                {data: 'name', name: 'name'},
                {data: 'feedback', name: 'feedback'},
//                {data: 'status', name: 'status'},
//                 {className: 'td-actions', data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    </script>
@endpush

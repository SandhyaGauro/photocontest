@if(isset($model))
    {{ Form::model($model, ['url' => route('admin.exhibitions.update', $model->id), 'method' => 'PUT','files' => true]) }}
@else
    {{ Form::open(['url' => route('admin.exhibitions.store'), 'method' => 'post', 'files' => true]) }}
@endif
<div class="grid simple ">
    <div class="grid-title">
        <h4>Exhibition Info</h4>
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>
        </div>
    </div>
    <div class="grid-body ">
        <div class="row">
            <div class="col-md-6 col-lg-6">
                <div class="form-group">
                    {!! Form::label('name', 'Name:', ['class' => 'form-label']) !!}
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
            <div class="col-md-6 col-lg-6">
                <div class="form-group">
                    {!! Form::label('slug', 'Slug:', ['class' => 'form-label']) !!}
                    {!! Form::text('slug', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('slug', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
            <div class="col-md-6 col-lg-6">
                <div class="form-group">
                    {!! Form::label('desc', 'Description:', ['class' => 'form-label']) !!}
                    {!! Form::textarea('desc',null, ['class' => 'form-control text-editor','id'=>'text-editor']) !!}
                    {!! $errors->first('desc', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>
            <div class="col-md-6 col-lg-6">
                <div class="form-group">
                    {!! Form::label('start_date', 'Start Date:', ['class' => 'form-label']) !!}
                    {!! Form::text('start_date', null, ['class' => 'form-control datepicker']) !!}
                    {!! $errors->first('start_date', '<div class="text-danger">:message</div>') !!}
                </div>

            </div>

        </div>

    </div>
</div>
<div class="grid simple ">
    <div class="grid-title">
        <h4>Exhibition Images<span><button type="button" class="btn btn-primary add_more">Add More</button></span></h4>
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>
        </div>
    </div>
    <?php $i = 1;
    ?>
    <div class="grid-body ">
        @if(isset($images) && $images->first() !=null)
            @foreach($images as $key=>$image)
                {!! Form::hidden('s_id[]',isset($image->id)?$image->id:null) !!}
                <div class="input_fields_wrap">
                    <div class="row">
                        <div class="col-md-3 col-lg-3">
                            <img src="{{url(isset($image)?$image->getImage():imageNotFound())}}" height="60"
                                 class="exhibitionimages_img">
                            {!! Form::label('slider', 'Image:') !!}
                            <small>Size: 1600*622 px</small>
                            <input type="file" class="exhibitionimages" name="exhibitionimages_image"
                                   data-class="input_fields_wrap">
                            <small id="slider_help_text" class="help-block"></small>
                            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0"
                                 aria-valuemax="100"
                                 aria-valuenow="0">
                                <div class="exhibitionimages_progress progress-bar progress-bar-success"
                                     style="width: 0%">
                                </div>
                            </div>

                        </div>
                        <input type="hidden" name="s_image[]"
                               class="exhibitionimages_path form-control exhibitionimages_progress"
                               value="{{isset($image)?$image->image:''}}"/>
                        {!! $errors->first('image', '<div class="text-danger">:message</div>') !!}
                        <div class="col-md-3 col-lg-3">
                            <div class="form-group">
                                {!! Form::label('s_caption', 'Caption:', ['class' => 'form-label']) !!}
                                {!! Form::text('s_caption[]',$image->caption, ['class' => 'form-control','required'=>'required']) !!}
                                {!! $errors->first('s_name', '<div class="text-danger">:message</div>') !!}
                            </div>

                        </div>


                        <div class="col-md-1 col-lg-1">
                            <br/>
                            <input type="button" class="delete-col" value="Remove">

                        </div>
                    </div>
                </div>
                @if($key!=0)
                    <?php $i++; ?>
                @endif
            @endforeach
        @else

            <div class="input_fields_wrap">
                <div class="row">
                    <div class="col-md-3 col-lg-3">
                        <img src="{{imageNotFound()}}" height="60" class="exhibitionimages_img">
                        {!! Form::label('slider', 'Image:') !!}
                        <small>Size: 1600*622 px</small>
                        <input type="file" class="exhibitionimages" name="exhibitionimages_image" data-class="input_fields_wrap">
                        <small id="slider_help_text" class="help-block"></small>
                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0"
                             aria-valuemax="100"
                             aria-valuenow="0">
                            <div class="exhibitionimages_progress progress-bar progress-bar-success" style="width: 0%">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="s_image[]"
                           class="exhibitionimages_path form-control exhibitionimages_progress"
                           value="{{isset($model)?$model->image:''}}"/>
                    {!! $errors->first('image', '<div class="text-danger">:message</div>') !!}
                    <div class="col-md-3 col-lg-3">
                        <div class="form-group">
                            {!! Form::label('s_caption', 'Caption:', ['class' => 'form-label']) !!}
                            {!! Form::text('s_caption[]', null, ['class' => 'form-control']) !!}
                            {!! $errors->first('s_name', '<div class="text-danger">:message</div>') !!}
                        </div>

                    </div>


                    <div class="col-md-1 col-lg-1">
                        <br/>
                        <input type="button" class="delete-col" value="Remove">

                    </div>
                </div>
            </div>
        @endif
    </div>
</div>


<div class="grid simple ">
    <div class="grid-title">
        <h4>Exhibition Image</h4>
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>
        </div>
    </div>

        <div class="grid-body ">
            <div class="row">


                <div class="col-md-12 col-lg-12">
                    @if(isset($model))
                        <img src="{{url(isset($model)?$model->getImage():imageNotFound())}}" height="250"
                             id="exhibition_img">

                    @else
                        <img src="{{isset($model)?$model->getImage():imageNotFound()}}" height="250"
                             id="exhibition_img">
                    @endif
                </div>

                <div class="form-group col-md-12 col-lg-12">
                    {!! Form::label('slider', 'Image:') !!}
                    <small>Size: 1600*622 px</small>
                    <input type="file" id="exhibition" name="exhibition_image"
                           onclick="anyFileUploader('exhibition')">
                    <small id="slider_help_text" class="help-block"></small>
                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0"
                         aria-valuemax="100"
                         aria-valuenow="0">
                        <div id="exhibition_progress" class="progress-bar progress-bar-success"
                             style="width: 0%">
                        </div>
                    </div>
                    <input type="hidden" id="exhibition_path" name="image" class="form-control"
                           value="{{isset($model)?$model->image:''}}"/>
                    {!! $errors->first('image', '<div class="text-danger">:message</div>') !!}
                </div>
            </div>


        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <input type="submit" value="Save" class="btn btn-primary"/>
            <a href="{{URL::previous()}}" class="btn btn-danger">Cancel</a>
        </div>
    </div>
</div>


{{ Form::close() }}


@push('scripts')
    <script>
        var i ={!!$i !!};
    </script>
    @include('backend.partials.common.file-upload');
    <script>
        function toggleFeature(tog,sel){
//        var tog='is_feature';
//        var sel=$('[name=is_feature]').val();
            $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
            $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
        }
        var wrapper = $(".input_fields_wrap"); //Fields wrapper
        var wrapper2 = $(".feature_wrap"); //Fields wrapper
        var add_button = $(".add_more"); //Add button ID

        var x =i;
        //initlal text box count

        $(add_button).click(function (e) { //on add input button click
            var cloned_shift = $(this).parents(".input_fields_wrap");
            cloned_shift = cloned_shift.clone(true);
            var cloned_shift = $(".input_fields_wrap:last")
            cloned_shift = cloned_shift.clone(true);
            cloned_shift.insertAfter(".input_fields_wrap:last");
            cloned_shift.find("input[type='text']").val("");
            cloned_shift.find("input[type='hidden']").val("");
            cloned_shift.find("input[type='file']").val("");
            cloned_shift.find("input[type='file']").val("");
            cloned_shift.find("img").attr('src','');
            cloned_shift.find('.progress2').css('width','0%')
            x++;
            // cloned_shift.insertAfter(".input_fields_wrap:last");
        });
        $(wrapper).on("click", ".delete-col", function (e) { //user click on remove text
            e.preventDefault();
            if (x != 1) {
                $(this).parents('.input_fields_wrap').remove();
                x--;
            }
        })
        $('.add_more2').click(function (e) { //on add input button click
            var cloned_shift = $(this).parents(".feature_wrap");
            cloned_shift = cloned_shift.clone(true);
            var cloned_shift = $(".feature_wrap:last")
            cloned_shift = cloned_shift.clone(true);
            cloned_shift.insertAfter(".feature_wrap:last");
            cloned_shift.find("input[type='text']").val("");
            cloned_shift.find("input[type='hidden']").val("");
            cloned_shift.find("input[type='file']").val("");
            cloned_shift.find("input[type='file']").val("");
            cloned_shift.find("img").attr('src','');
            cloned_shift.find('.progress2').css('width','0%')
            cloned_shift.find('.featurevideo_img').attr('src','');
            y++;
            // cloned_shift.insertAfter(".input_fields_wrap:last");
        });
        $(wrapper2).on("click", ".delete-col2", function (e) { //user click on remove text
            e.preventDefault();
            if (y != 1) {
                $(this).parents('.feature_wrap').remove();
                y--;
            }
        })
        $('.add_more3').click(function (e) { //on add input button click
            var cloned_shift = $(this).parents(".colour_wrap");
            cloned_shift = cloned_shift.clone(true);
            var cloned_shift = $(".colour_wrap:last")
            cloned_shift = cloned_shift.clone(true);
            cloned_shift.insertAfter(".colour_wrap:last");
            cloned_shift.find("input[type='text']").val("");
            cloned_shift.find("input[type='hidden']").val("");
            cloned_shift.find("input[type='file']").val("");
            cloned_shift.find("input[type='file']").val("");
            cloned_shift.find("img").attr('src','https://via.placeholder.com/350x150');
            cloned_shift.find('.progress2').css('width','0%')
//        cloned_shift.find('.featurevideo_img').attr('src','');
            cloned_shift.find('.add_colour_images').remove()
            z++;
            // cloned_shift.insertAfter(".input_fields_wrap:last");
        });
        $('.colour_wrap').on("click", ".delete-col3", function (e) {
            console.log(z);//user click on remove text
            e.preventDefault();
            if (z != 1) {
                $(this).parents('.colour_wrap').remove();
                z--;
            }
        })
    </script>

    <script>

        $('.exhibitionimages').click(function () {
            var parent_class = $(this).attr('data-class');
            console.log(parent_class);
            var id = $(this).attr('class');
            console.log($(this).attr('class'));
            var parents = $(this).parents('.' + parent_class)
            parents.find('.' + id + '_progress').css('width', '0%');
            $('.' + id).fileupload({
                url: '{{ url('dashboard/ajax/any-file-upload') }}' + '/' + id,
                acceptFiles: "video/*",
                maxFileSize: 40000000,
                done: function (e, data) {
                    console.log(data);
                    console.log(data.result.full_url);
                    parents.find('.' + id + '_help_text').text('File Upload Successfully');
                    parents.find('.' + id + '_path').val(data.result.image_name);
                    parents.find('.' + id + '_img').attr('src', data.result.full_url);
                    parents.find('.' + id + '_progress').parent().removeClass('progress-striped');
                },
                error: function (e, data) {
                    parents.find('.' + id + '_help_text').text(eval('e.responseJSON.' + id + '_image')[0]);
                    parents.find('.' + id + '_progress').css('width', '0%');
                    console.log(e.responseText);
                },
                progress: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    parents.find('.' + id + '_progress').css('width', progress + '%');
                }
            });
        });

    </script>
@endpush

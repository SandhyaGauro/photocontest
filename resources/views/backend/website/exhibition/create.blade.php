@extends('backend.layout.app')

@section('content')
    @include('backend.partials.common.page-title', ['page_title' => 'Create New Exhibition'])
    <div>
        <div class="row-fluid">
            <div class="span12">
                @include('backend.website.exhibition.form')
            </div>
        </div>
    </div>
@endsection

@push('scripts')

@endpush
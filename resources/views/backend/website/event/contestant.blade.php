@extends('backend.layout.app')

@section('content')
    @include('backend.partials.common.page-title', ['page_title' => ' Contestant List'])


    <div>
        <div class="row-fluid">
            <div class="span12">
                <div class="grid simple ">
                    <div class="grid-title">
                        {{--<a href="{{route('event.create')}}"  class="btn btn-info btn-cons">--}}
                        {{--<i class="fa fa-plus-square"></i> Add New--}}
                        {{--</a>--}}
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="grid-body ">
                        <table class="table table-hover table-condensed" id="data-table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Contact</th>
                                <th>vote</th>
                                <th>is winner</th>
                                <th>image type</th>
                                <th>image</th>
                                <th class="disabled-sorting">Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $('#data-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('event.contestant.index',$id) }}',
            columns: [
                {data: 'id', name: 'id'},
//                {data: 'name', name: 'name'},
                {data: 'name', name: 'name'},
                {data: 'address', name: 'address'},
                {data: 'contact_no', name: 'contact_no'},
                {data: 'vote', name: 'vote'},
                {data: 'is_winner', name: 'is_winner'},
                {data: 'image_type', name: 'image_type'},
                {data: 'image', name: 'image'},
                // {data: 'event_image', name: 'event_image'},
//                {data: 'status', name: 'status'},
                {className: 'td-actions', data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    </script>
@endpush

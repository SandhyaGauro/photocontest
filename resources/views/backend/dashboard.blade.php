@extends('backend.layout.app')

@section('content')
    <div>
        <div class="row 2col">
            <div class="col-md-3 col-sm-6 spacing-bottom-sm spacing-bottom">
                <div class="tiles blue added-margin">
                    <div class="tiles-body">
                        <div class="controller">
                        </div>
                        <div class="tiles-title"> Dashboard</div>
                        <div class="heading"><a href="{{ url('admin/event/create') }}"  style="color: white">  Add Events </a></div>
                        <div class="progress transparent progress-small no-radius">
                            <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="100%"></div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 spacing-bottom-sm spacing-bottom">
                <div class="tiles green added-margin">
                    <div class="tiles-body">
                        <div class="controller">
                        </div>
                        <div class="tiles-title"> Dashboard</div>
                        <div class="heading"><a href="{{ url('admin/exhibitions/create') }}"  style="color: white">Add Exhibittion </a></div>
                        <div class="progress transparent progress-small no-radius">
                            <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="100%"></div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 spacing-bottom-sm spacing-bottom">
                <div class="tiles red added-margin">
                    <div class="tiles-body">
                        <div class="controller">
                        </div>
                        <div class="tiles-title"> Dashboard</div>
                        <div class="heading"><a href="{{ url('admin/event/create') }}"  style="color: white">  Event Create </a></div>
                        <div class="progress transparent progress-small no-radius">
                            <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="100%"></div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection